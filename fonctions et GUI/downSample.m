function V=downSample (Val)
	[r,c]=size(Val);
	y=1:2:c;                   
	x=1:2:r;
	V=Val(x,y); 
       
end