
                %-------------------------------%
                % Extraire les infos de l'image %
                %-------------------------------%
                
function infos = lire(imageName)
infos = [];                         % Initialiser infos.


info = imfinfo(imageName);
infos.hauteur = info.Height;
infos.largeur = info.Width;
infos.bitsParPixel = info.BitDepth;
if infos.bitsParPixel > 8
    [I, map] = rgb2ind(imread(imageName), 256);
    infos.map = map;
else
    [I, map] = imread(imageName);
    if isempty(map)
        infos.map = [];
    else
        if length(map) < 256
            infos.map = [];
        else
            infos.map = map;
        end
    end
end

infos.image = I;
end