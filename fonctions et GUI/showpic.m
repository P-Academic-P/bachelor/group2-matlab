function showpic (s)
	sNtscT(:,:,1) = s.picY;
	sNtscT(:,:,2) = s.picI;
	sNtscT(:,:,3) = s.picQ;
	newColorS = ntsc2rgb(sNtscT);
	imshow(newColorS);
    imwrite(s, 'Pic.jpg', 'JPEG');
end