
                %------------------------------%
                % Appliquer RLE sur un vecteur %
                %------------------------------%
                
% Cette fonction applique RLE sur un vecteur quelconque et retourne les
% symboles et leurs occurences dans des vecteurs diff�rents. (Utilis�e dans
% la fonction compresser).

function [symbols, occur] = rle(v)
    if size(v,1) > size(v,2)
        v = v';
    end
    largeur = length(v);
    s1 = v(1 : largeur - 1) ~= v(2 : largeur);   % v�rifier si v(i) == v(i + 1) pour 1 < i < largeur.
    s2 = [find(s1) largeur];                     % Retourner les indices des valeurs non nulles.
    symbols = v(s2);                             % extraire les symbols.
    occur = diff([0 s2]);                        % extraire leurs occurences.
end