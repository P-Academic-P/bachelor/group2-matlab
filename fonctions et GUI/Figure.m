function varargout = Figure(varargin)
%FIGURE M-file for Figure.fig
%      FIGURE, by itself, creates a new FIGURE or raises the existing
%      singleton*.
%
%      H = FIGURE returns the handle to a new FIGURE or the handle to
%      the existing singleton*.
%
%      FIGURE('Property','Value',...) creates a new FIGURE using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Figure_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      FIGURE('CALLBACK') and FIGURE('CALLBACK',hObject,...) call the
%      local function named CALLBACK in FIGURE.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Figure

% Last Modified by GUIDE v2.5 03-Apr-2016 22:03:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Figure_OpeningFcn, ...
                   'gui_OutputFcn',  @Figure_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Figure is made visible.
function Figure_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for Figure
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Figure wait for user response (see UIRESUME)
% uiwait(handles.gui);


% --- Outputs from this function are returned to the command line.
function varargout = Figure_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Chemin_Callback(hObject, eventdata, handles)
% hObject    handle to Chemin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Chemin as text
%        str2double(get(hObject,'String')) returns contents of Chemin as a double


% --- Executes during object creation, after setting all properties.
function Chemin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Chemin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Choix.
function Choix_Callback(hObject, eventdata, handles)
% hObject    handle to Choix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Choix contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Choix
if get(hObject,'Value') == 2
    set(handles.slider, 'visible', 'on')
    set(handles.text_Qualite, 'visible', 'on')
    set(handles.q, 'visible', 'on')
else
    set(handles.slider, 'visible', 'off')
    set(handles.text_Qualite, 'visible', 'off')
    set(handles.q, 'visible', 'off')
end
        

% --- Executes during object creation, after setting all properties.
function Choix_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Choix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in B_Compresser.
function B_Compresser_Callback(hObject, eventdata, handles)
% hObject    handle to B_Compresser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

valeur = get(handles.Choix, 'Value');
fullPath = get(handles.Chemin, 'string');
if isempty(fullPath)
    warndlg('Veuillez choisir une image.', 'Avertissement');
else
%     h = waitbar(0,'Please wait...');
%     steps = 1000;
%     for step = 1:steps
%         waitbar(step / steps)
%         pause(.0001)
%     end

    drawnow;
    
    switch valeur
        case 1
            set(handles.text, 'string', 'Veuillez patienter...');
            set(handles.text, 'visible', 'on');
            drawnow;
            infos = lire(fullPath);
            entete(infos);
            compresser(infos.image);
            d1 = dir(fullPath);
            d2 = dir('Pic.bmp');
            subplot(1, 2, 1)
            imshow(fullPath)
            title(['Image d''origine - ' num2str(d1.bytes) ' octets'],'fontsize',12);
            subplot(1, 2, 2)
            imshow('Pic.bmp')
            title(['Image compressée - ' num2str(d2.bytes) ' octets'],'fontsize',12);
            set(handles.text, 'visible', 'off');
            drawnow;
            set(handles.Made, 'visible', 'off');
            set(handles.taux, 'string', ['Taux de comp : ' num2str(((d1.bytes - d2.bytes)/d1.bytes) * 100) ' %']);
            if infos.bitsParPixel == 1
                set(handles.MSE, 'string', ['MSE : 0' ]);
            else
                set(handles.MSE, 'string', ['MSE : ' num2str(comparer(infos.image, imread('Pic.bmp')))]);
            end
            set(handles.taux, 'visible', 'on');
            set(handles.MSE, 'visible', 'on');
        case 2
            I = imread(fullPath);
            if length(size(I)) < 3
                warndlg('Veuillez choisir une image RGB.', 'Avertissement');
                set(handles.text, 'visible', 'off');
            else
                set(handles.text, 'string', 'Veuillez patienter...');
                set(handles.text, 'visible', 'on');
                drawnow;
                q = get(handles.slider,'Value');
                s = jpeg(I, q, 0);
                
                sNtscT(:,:,1) = s.picY;
                sNtscT(:,:,2) = s.picI;
                sNtscT(:,:,3) = s.picQ;
                newColorS = ntsc2rgb(sNtscT);
                imwrite(newColorS,'Pic.jpg', 'JPEG');
                
                d1 = dir(fullPath);
                d2 = dir('Pic.jpg');
                subplot(1, 2, 1)
                imshow(fullPath)
                title(['Image d''origine - ' num2str(d1.bytes) ' octets'],'fontsize',12);
                subplot(1,2,2);
                imshow(newColorS);
                title(['Image compressée - ' num2str(d2.bytes) ' octets'],'fontsize',12);
                set(handles.text, 'visible', 'off');
                set(handles.Made, 'visible', 'off');
                set(handles.taux, 'string', ['Taux de comp : ' num2str(((d1.bytes - d2.bytes)/d1.bytes) * 100) ' %']);
                set(handles.MSE, 'string', ['MSE : ' num2str(sum(comparer(I, imread('Pic.jpg'))) / 3)]);
                set(handles.taux, 'visible', 'on');
                set(handles.MSE, 'visible', 'on');
                drawnow;
            end
    end

end

% --- Executes on button press in Parcourir.
function Parcourir_Callback(hObject, eventdata, handles)
% hObject    handle to Parcourir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.text, 'visible', 'off');
[filename, pathname] = uigetfile({'*.png','Image(*.png)'}, 'Choisir une image', 'Multiselect', 'off');
if  isequal(filename, 0)
    set(handles.Chemin, 'string', '');
else
    set(handles.taux, 'visible', 'off');
    set(handles.MSE, 'visible', 'off');
    set(handles.Made, 'visible', 'on');
    set(handles.Chemin, 'string', [pathname filename]);
    subplot(1, 1, 1)
    imshow([pathname filename]);
end


% --- Executes on button press in quitter.
function quitter_Callback(hObject, eventdata, handles)
% hObject    handle to quitter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.gui);

% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.q,'String',...
    num2str(get(handles.slider,'Value')));



% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
