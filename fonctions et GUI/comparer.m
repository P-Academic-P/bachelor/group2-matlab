function MSE = comparer(imageNonComp, imageComp)
hauteur = size(imageNonComp, 1);
largeur = size(imageNonComp, 2);
MSE = sum(sum((imageNonComp - imageComp).^2)) / (hauteur * largeur);