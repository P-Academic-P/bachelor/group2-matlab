
                %-----------------------------------------%
                % Compresser une image en utilisant RLE-8 %
                %-----------------------------------------%
                
% Cette fonction applique une compression RLE-8 sur l'image I ligne par
% ligne.

function compresser(I)

% Ouvrir le m�me fichier ou on a enregistr� l'en-t�te.
F = fopen('Pic.bmp', 'ab');

% Extraire la taille de l'image.
largeur = size(I, 2);
hauteur = size(I, 1);

for j = hauteur : -1 : 1
    % Appliquer RLE sur la ligne n� j de l'image.
    v = I(j, :);
    [symbols, occur] = rle(v);
    
    % ABSOLUTE MODE.
    k = 1;
    while k <= length(symbols)
        while occur(k) > 255
            fwrite(F, 255, 'uint8');
            fwrite(F, symbols(k), 'uint8');
            occur(k) = occur(k) - 255;
        end
        if occur(k) < 4
            y = [];
            while (k <= length(symbols) && occur(k) < 4)
                if occur(k) == 2
                    y = [y symbols(k) symbols(k)];
                    k = k + 1;
                else if occur(k) == 1
                        y = [y symbols(k)];
                        k = k + 1;
                    else
                        y = [y symbols(k) symbols(k) symbols(k)];
                        k = k + 1;
                    end
                end
            end
            % Ecriture dans le fichier.
            max = length(y);
            while max > 255
                fwrite(F, 0, 'uint8');
                fwrite(F, 255, 'uint8');
                
                for l = 1 : 255
                    fwrite(F, y(l), 'uint8');
                end
                fwrite(F, 0, 'uint8');
                y = y(256:end);
                max = length(y);
            end
            if max == 2
                if y(1) == y(2)
                    fwrite(F, 2, 'uint8');
                    fwrite(F, y(1), 'uint8');
                else
                    fwrite(F, 1, 'uint8');
                    fwrite(F, y(1), 'uint8');
                    fwrite(F, 1, 'uint8');
                    fwrite(F, y(2), 'uint8');
                end
            else if max == 1
                    fwrite(F, 1, 'uint8');
                    fwrite(F, y(1), 'uint8');
                else
                    fwrite(F, 0, 'uint8');
                    fwrite(F, max, 'uint8');
                    for l = 1 : max
                        fwrite(F, y(l), 'uint8');
                    end
                end
            end
            if max > 2 && mod(max, 2) ~= 0
                fwrite(F, 0, 'uint8');
            end
            
        else
            fwrite(F, occur(k), 'uint8');
            fwrite(F, symbols(k), 'uint8');
            k = k + 1;
        end
    end
    
    if j == 1
        % Ecrire 1 (00 01 en h�x) indique la fin de l'image.
        fwrite(F, 0, 'uint8');
        fwrite(F, 1, 'uint8');
    else
        % Ecrire 0 (00 00 en h�x) indique la fin de la ligne.
        fwrite(F, 0, 'uint8');
        fwrite(F, 0, 'uint8');
    end
end
fclose(F);
end