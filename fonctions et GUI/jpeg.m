function [s]=jpeg(sBmp,quality,showDetail)
%implementation of JPEG compression
set(0, 'DefaultFigureColor', 'White', ...
'DefaultTextFontSize', 15, ...
'DefaultTextFontWeight', 'Bold');
imshow(sBmp);
[r,c,m]=size(sBmp);

% when 4 down sample also must be os 8*8 block
r4 = rem(r,32);
c4 = rem(c,32);
r1 = r-r4;
c1 = c-c4;

sBmp=sBmp(1:r1,1:c1,:);

% subplot(1,2,1);
% imshow(sBmp);
% title('Image D''origine', 'fontsize',12);

% convert RGB image into YIQ space
sNtsc = rgb2ntsc(sBmp);
picY = sNtsc(:,:,1)*256-128; % Y luminance component
picI = sNtsc(:,:,2)*256-128; % I chrominance component
picQ = sNtsc(:,:,3)*256-128; % Q chrominance component

x  =struct('picY',picY,'picI',picI,'picQ',picQ);
x2 =struct('picY',downSample(picY),'picI',downSample(picI),'picQ',downSample(picQ));%downsample 2 of x
x4 =struct('picY',downSample(downSample(picY)),'picI',downSample(downSample(picI)),'picQ',downSample(downSample(picQ)));%downsample 4 of x
      
QL=[16,  11,  10,  16,  24,  40,  51,  61,
    12,  12,  14,  19,  26,  58,  60,  55,
	14,  13,  16,  24,  40,  57,  69,  56,
    14,  17,  22,  29,  51,  87,  80,  62,
	18,  22,  37,  56,  68, 109, 103,  77,
	24,  35,  55,  64,  81, 104, 113,  92,
    49,  64,  78,  87, 103, 121, 120, 101,
	72,  92,  95,  98, 112, 100, 103,  99 ];
QC=[17,  18,  24,  47,  99,  99,  99,  99, 
    18,  21,  26,  66,  99,  99,  99,  99,
	24,  26,  56,  99,  99,  99,  99,  99,
    47,  66,  99,  99,  99,  99,  99,  99,
	99,  99,  99,  99,  99,  99,  99,  99,
	99,  99,  99,  99,  99,  99,  99,  99,
    99,  99,  99,  99,  99,  99,  99,  99,
	99,  99,  99,  99,  99,  99,  99,  99 ];

qf=quality;
if qf>95
    qscale=1;

elseif qf < 50
qscale = floor(5000 / qf ) ;
else
qscale = 200 - 2 * qf ;
end
QC = (QC*(qscale/100));		
QL = (QL*(qscale/100));	
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%encode x4
	%applyDCT();
	fun = @dct2;
	picTrY = blkproc(x4.picY, [8 8], fun);
	picTrI = blkproc(x4.picI, [8 8], fun);
	picTrQ = blkproc(x4.picQ, [8 8], fun);
	%applyQuantize
	quantizedY=round(blkproc(picTrY,[8 8],'divq',QL));
	quantizedI=round(blkproc(picTrI,[8 8],'divq',QC));
	quantizedQ=round(blkproc(picTrQ,[8 8],'divq',QC));
    s1=struct('quantizedY',quantizedY,'quantizedI',quantizedI,'quantizedQ',quantizedQ); 
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%decode x4
	%applyDeQuantize
	iquantizedY=blkproc(quantizedY,[8 8],'mulq',QL);
	iquantizedI=blkproc(quantizedI,[8 8],'mulq',QC);
	iquantizedQ=blkproc(quantizedQ,[8 8],'mulq',QC);
	%applyIDCT
	decFun = @idct2;
	newPicY = blkproc(iquantizedY, [8 8], decFun);
	newPicI = blkproc(iquantizedI, [8 8], decFun);
	newPicQ = blkproc(iquantizedQ, [8 8], decFun);
    %scale
    newPicY = (newPicY+128)/256;
	newPicI = (newPicI+128)/256;
	newPicQ = (newPicQ+128)/256;
    
    x4ed=struct('picY',upSample(newPicY),'picI',upSample(newPicI),'picQ',upSample(newPicQ));
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%encode diff1,diff1= x2-upsample (decode x4)
dif1=struct('picY',(x2.picY+128)/256-x4ed.picY,'picI',(x2.picI+128)/256-x4ed.picI,'picQ',(x2.picQ+128)/256-x4ed.picQ);
   
	%applyDCT();
	fun = @dct2;
	picTrY = blkproc(dif1.picY, [8 8], fun);
	picTrI = blkproc(dif1.picI, [8 8], fun);
	picTrQ = blkproc(dif1.picQ, [8 8], fun);
	%applyQuantize
	quantizedY=round(blkproc(picTrY,[8 8],'divq',QL));
	quantizedI=round(blkproc(picTrI,[8 8],'divq',QC));
	quantizedQ=round(blkproc(picTrQ,[8 8],'divq',QC));
    s2=struct('quantizedY',quantizedY,'quantizedI',quantizedI,'quantizedQ',quantizedQ); 


	%applyDeQuantize
	iquantizedY=blkproc(quantizedY,[8 8],'mulq',QL);
	iquantizedI=blkproc(quantizedI,[8 8],'mulq',QC);
	iquantizedQ=blkproc(quantizedQ,[8 8],'mulq',QC);
	%applyIDCT
	decFun = @idct2;
	newPicY = blkproc(iquantizedY, [8 8], decFun);
	newPicI = blkproc(iquantizedI, [8 8], decFun);
	newPicQ = blkproc(iquantizedQ, [8 8], decFun);

     
    x2ed=struct('picY',upSample(x4ed.picY+newPicY),'picI',upSample(x4ed.picI+newPicI),'picQ',upSample(x4ed.picQ+newPicQ));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%encode diff2,diff2 = x-upsample (decode x2)
dif2=struct('picY',(x.picY+128)/256-x2ed.picY,'picI',(x.picI+128)/256-x2ed.picI,'picQ',(x.picQ+128)/256-x2ed.picQ);
   
    %applyDCT();
	fun = @dct2;
	picTrY = blkproc(dif2.picY, [8 8], fun);
	picTrI = blkproc(dif2.picI, [8 8], fun);
	picTrQ = blkproc(dif2.picQ, [8 8], fun);
	%applyQuantize
	quantizedY=round(blkproc(picTrY,[8 8],'divq',QL));
	quantizedI=round(blkproc(picTrI,[8 8],'divq',QC));
	quantizedQ=round(blkproc(picTrQ,[8 8],'divq',QC));
    s3=struct('quantizedY',quantizedY,'quantizedI',quantizedI,'quantizedQ',quantizedQ); 

%**************DECODE
 
%decode s1
	%applyDeQuantize
	iquantizedY=blkproc(s1.quantizedY,[8 8],'mulq',QL);
	iquantizedI=blkproc(s1.quantizedI,[8 8],'mulq',QC);
	iquantizedQ=blkproc(s1.quantizedQ,[8 8],'mulq',QC);
	%applyIDCT
	decFun = @idct2;
	newPicY = blkproc(iquantizedY, [8 8], decFun);
	newPicI = blkproc(iquantizedI, [8 8], decFun);
	newPicQ = blkproc(iquantizedQ, [8 8], decFun);
    %scale
    newPicY = (newPicY+128)/256;
	newPicI = (newPicI+128)/256;
	newPicQ = (newPicQ+128)/256;
    sd1=struct('picY',newPicY,'picI',newPicI,'picQ',newPicQ);
%decode s2
	%applyDeQuantize
	iquantizedY=blkproc(s2.quantizedY,[8 8],'mulq',QL);
	iquantizedI=blkproc(s2.quantizedI,[8 8],'mulq',QC);
	iquantizedQ=blkproc(s2.quantizedQ,[8 8],'mulq',QC);
	%applyIDCT
	decFun = @idct2;
	newPicY = blkproc(iquantizedY, [8 8], decFun);
	newPicI = blkproc(iquantizedI, [8 8], decFun);
	newPicQ = blkproc(iquantizedQ, [8 8], decFun);
    sd2=struct('picY',newPicY,'picI',newPicI,'picQ',newPicQ);

%decode s3
	%applyDeQuantize
	iquantizedY=blkproc(s3.quantizedY,[8 8],'mulq',QL);
	iquantizedI=blkproc(s3.quantizedI,[8 8],'mulq',QC);
	iquantizedQ=blkproc(s3.quantizedQ,[8 8],'mulq',QC);
	%applyIDCT
	decFun = @idct2;
	newPicY = blkproc(iquantizedY, [8 8], decFun);
	newPicI = blkproc(iquantizedI, [8 8], decFun);
	newPicQ = blkproc(iquantizedQ, [8 8], decFun);

    
    sd3=struct('picY',newPicY,'picI',newPicI,'picQ',newPicQ);
    

    newPicY=upSample(upSample(sd1.picY)+sd2.picY)+sd3.picY;
    newPicI=upSample(upSample(sd1.picI)+sd2.picI)+sd3.picI;
    newPicQ=upSample(upSample(sd1.picQ)+sd2.picQ)+sd3.picQ;

s=struct('picY',newPicY,'picI',newPicI,'picQ',newPicQ);
   
