
                %----------------------------------%
                % Ecrire l'en-t�te d'une image bmp %
                %----------------------------------%

function entete(infos)

F = fopen('Pic.bmp', 'wb');
                                    % BMP header.
        % Le nombre des octets qu'on doit ajouter pour que chaque ligne
        % soit divisible par 4.
        % extraOctets = mod((4 - mod(infos.largeur, 4)), 4);
        if infos.bitsParPixel > 8
            infos.bitsParPixel = 8;
        end
        % La taille totale de l'image(l'en-t�te n'est pas inclus).
        imageTaille = infos.largeur * infos.hauteur;
        fileTaille = imageTaille + 54 + 256 * 4;
        % BMP file header.
        fwrite(F, 'B', 'uint8');                        % 'B' (1 octet).
        fwrite(F, 'M', 'uint8');                        % 'M' (1 octet).
        fwrite(F, fileTaille, 'uint32');                % Taille (4 octets).
        fwrite(F, 0, 'uint32');                         % Reserv� (4 octets).
        fwrite(F, 54 + 256*4, 'uint32');                % offset (4 octets).

        % BMP image header.
        fwrite(F, 40, 'uint32');                        % Taille de l'entete en octets (4 octets).
        fwrite(F, infos.largeur, 'uint32');             % Largeur (4 octets).
        fwrite(F, infos.hauteur, 'uint32');             % Hauteur (4 octets)(n�gative pour un normal ordre).
        fwrite(F, 1, 'uint16');                         % Toujours 1 (2 octets).
        fwrite(F, 8, 'uint16');                         % Nombre de bits par pixel (2 octets).
        fwrite(F, 1, 'uint32');                         % Compression? (0 non, 1 RLE-8, 2 RLE-4) (4 octets).
        fwrite(F, 0, 'uint32');                         % Taille de l'image (4 octets).
        fwrite(F, 2835, 'uint32');                      % Resolution de
        fwrite(F, 2835, 'uint32');                      % l'image (8 octets).
        fwrite(F, 256, 'uint32');                         % Nombre de couleurs dans la palette(4 octets).
        fwrite(F, 0, 'uint32');                         % Tous les couleurs sont importants(4 octets).

        % Palette.
        if isempty(infos.map) % si l'image ne poss�de pas d'une palette.
            if infos.bitsParPixel == 8 % si elle est en niveau de gris.
                for i = 0 : 2^(infos.bitsParPixel) - 1
                    for j = 1 : 3
                        fwrite(F, i, 'uint8');
                    end
                  fwrite(F, 0, 'uint8');
                end
            else % si elle est binaire.
                for k = 1 : 2 :2^8 - 1
                    for i = 0 : 1
                        for j = 1 : 3
                            fwrite(F, i * 255, 'uint8');
                        end
                        fwrite(F, 0, 'uint8');
                    end
                end
            end
        else % si elle contient d�j� une palette.
            for i = 1 : length(infos.map)
                for j = 3 : -1 : 1
                    fwrite(F, infos.map(i, j) * 255, 'uint8');
                end
		      fwrite(F, 0, 'uint8');
            end 
        end

fclose(F);