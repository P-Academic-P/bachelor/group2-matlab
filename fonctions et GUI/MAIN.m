
                                %------%
                                % Main %
                                %------%

% Choisir l'image.
[filename, pathname] = uigetfile({'*.png','Image(*.png)'}, 'Choisir une image', 'Multiselect', 'off');
if  isequal(filename, 0)
    warndlg('vous n''avez rien choisi :/');
    
else
    % le chemin complet.
    fullPath = [pathname filename];

    % Determiner les caractéristiques de l'image.
    infos = lire(fullPath);

    % Préparer l'entête.
    entete(infos);

    % Compresser l'image.
    compresser(infos.image);
    imshow('Pic.bmp');
end

